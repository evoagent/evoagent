package main;

// these should be the only two classes in the src folder that are imported
import main.agent.Simulation;
import main.evointerface.Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by daniels on 22/06/2015.
 */
public class Evo {

    Interface evoPanel;
    Simulation simulation;

    private HashMap<String,String> parameterValues;

    private int gridSize;
    private int noOfSteps;
    private int maxDiffusionRate;
    private Random rgen;

    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Evo controller = new Evo();
                controller.create_and_show_GUI();

            }
        });
    }

    public Evo()
    {
        evoPanel = new Interface();
        parameterValues = evoPanel.getParameterValues();
        System.out.println("Got here");
        create_simulation();
        int gridSize = extract_integer(parameterValues.get("grid_size"));
        int[][] colorArray = new int[gridSize][gridSize];
        colorArray = simulation.showInitialGrid();
        evoPanel.updateGrid(colorArray);
    }


    private void create_and_show_GUI(){


        evoPanel.startButton.addActionListener(
                new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        System.out.println("Pressed start " + e);

                        start_simulation();
                        /* These variables contain the information from the Swing GUI */


                        /* cellinfo is the main array that should be modified in order to change
                           what is shown in the GUI
                           */

                        /*
                        for (int k = 0; k < 100; k++)
                        {
                            int[][] cellinfo = new int[grid_size][grid_size];
                            for (int i = 0; i < evoPanel.get_grid_size(); i++) {
                                for (int j = 0; j < evoPanel.get_grid_size(); j++) {
                                    cellinfo[i][j] = ((i % 3) + k) % 3 + 1;
                                }
                            }
                            evoPanel.updateGrid(cellinfo);
                        }
                        */

                        // updateGrid will change the grid colors according to the 2D array passed

                    }
                });
    }

    // Create a new Simulation object (nothing special here)
    private void create_simulation(){
        //System.out.println(parameterValues.get("random_seed"));
        rgen = new Random(extract_integer(parameterValues.get("random_seed")));
        gridSize = extract_integer(parameterValues.get("grid_size"));
        noOfSteps = extract_integer(parameterValues.get("steps"));
        maxDiffusionRate = extract_integer(parameterValues.get("max_diffusion"));
        simulation = new Simulation(rgen,
                                    noOfSteps,
                                    extract_integer(parameterValues.get("trials")),
                                    extract_integer(parameterValues.get("maxJ")),
                                    extract_double(parameterValues.get("min_fitness")),
                                    gridSize,
                                    extract_integer(parameterValues.get("signal_peptides")),
                                    extract_integer(parameterValues.get("output_peptides")),
                                    extract_integer(parameterValues.get("no_of_agents")),
                                    extract_integer(parameterValues.get("max_output")),
                                    extract_integer(parameterValues.get("max_sensitivity")),
                                    maxDiffusionRate,
                                    extract_integer(parameterValues.get("max_colours")));
    }

    // Runs the simulation
    private void start_simulation(){
        System.out.println("Starting simulation");
        //System.out.println("start_simulation in EDT: " + SwingUtilities.isEventDispatchThread());
        SimulationWorker worker = new SimulationWorker();
        worker.execute();


    }

    private void print_array(int[][] arr)
    {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++)
                System.out.print("" + arr[i][j] + " ");
            System.out.print('\n');
        }


    }

    private class SimulationWorker extends SwingWorker<Integer,String>{

        public SimulationWorker() {

        }

        @Override
        protected Integer doInBackground() throws Exception {

            for(int i = 0; i < noOfSteps; i++) {
                int[][] colorArray = simulation.update();//rgen,maxDiffusionRate);
                evoPanel.updateGrid(colorArray);
                setProgress((i+1)*100/noOfSteps);
            }
            return 1;
        }

        @Override
        protected void process(List<String> chunks) {
        }
    }

    private int extract_integer(String s){
        String value = s.replaceAll("\\D","");
        if (value.isEmpty())
            return 0;
        else
            return Integer.parseInt(value);
    }
    private double extract_double(String s){
        String value = s.replaceAll("\\D","");
        if (value.isEmpty())
            return 0;
        else
            return Double.parseDouble(value);
    }
}




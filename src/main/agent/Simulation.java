package main.agent;/*-------------------------------------------------------------------------------------------------------------------*\
|                                                 Class main.agent.Simulation                                                      |
|                                     Main class controlling the simulation                                           |
|                                         Author: Jennifer Hallinan                                                   |
|                                          Commenced: 20/10/2014                                                      |
|                                          Last edited: 28/04/2015                                                    |
\*-------------------------------------------------------------------------------------------------------------------*/

import main.evointerface.Interface;

import java.util.*;

public class Simulation {

    // Class variables from user input
    private Random rgen = new Random(571);
    private int numberOfSteps = 200;                               // number of timesteps to run the algorithm for
    private int numberOfTrials;                               // number of times to run outer loop
    private int maxJ = 1000;                                // fall back stopping ccriterion
    private double minFitness = 0;                               // stopping criterion; minimum fitness

    public int gridSize = 51;


    private int maxSignalPeptides = 100;                               // maximum number of output signal peptide molecules
    private int maxReporterPeptides = 50;                               // maximum number of output reporter peptide molecules
    private int numberOfAgents = 50;                              // number of agents

    private int maxOutput;
    private int maxSensitivity = 2;                          // maximum number of peptides needed to trigger an main.agent
    private int maxDiffusionRate = 15;                                   // maximum diffusion rate per timestep
    private int maxColours = 3;                              // number of output colours produced
    private int maxColonyDepth = 4;
    // Class variables to be computed from other class variables, computed as gridSize * gridSize
    private int startingSignalPeptides = 150;             // approximately one signal peptide per square, to start
    private int startingOutputPeptides = 0;             // same with output peptides


    // private variables
    public ArrayList<Agent> agents;
    private ArrayList<Peptide> peptides;
    private ArrayList<Integer> target;
    public static Grid grid;

    private int moveAttempts = 0;
    private int timeSteps =  10;

    public static void main(String args[]) {

        Simulation temp = new Simulation();
        temp.simulate();
    }

    public int[][] showInitialGrid() {
        System.out.println("Initial grid");
        return grid.getColorArray(agents);
    }

    public void simulate() {
        Interface tempInterface = new Interface();
        tempInterface.changeGridSize(gridSize);
        // set up the EA
        System.out.println("x \t y \t Sensitivity \t Motility \t No. signal \t No. reporter \t Colour");
        printAgentsString();

        // run for numsteps
        for (int i = 0; i < numberOfSteps; i++) {
            // System.out.println("Step " + i);
            update();//rgen, maxDiffusionRate);
        }
        System.out.println("Full reporter grid before first fitness assessment:");
        grid.printReporterGrid();
        System.out.println("Summary grid before first fitness assessment");
        grid.printColourGrid();

        // calculate fitness
        double parentFitness = calcFitness();
        System.out.println("Initial fitness: " + parentFitness);

        // copy agents to oldagents
        ArrayList<Agent> oldAgents = copyAgents();
        System.out.println("Old agents (as copied to new ArrayList:");
        for (int i = 0; i < oldAgents.size(); i++) {
            Agent x = oldAgents.get(i);
            x.printAgentString();
        }

        // run until mindiff is met or a certain number of trials
        int j = 0;
        while ((parentFitness < minFitness) || (j < maxJ)) {
            //System.out.println("Outer loop step " + j);
            // mutate agents
            mutate(rgen, maxSensitivity, maxDiffusionRate, maxSignalPeptides, maxReporterPeptides, maxColours);

            // run for numsteps
            for (int i = 0; i < numberOfSteps; i++) {
                //System.out.println("Inner loop step " + i);
                if(j%timeSteps == 0){
                 tempInterface.updateGrid( update());//rgen, maxDiffusionRate));
                }
                // System.out.println("colour grid at " + i + " steps");
                // grid.printColourGrid();
                // grid.printAgentGrid();


            }

            // calculate fitness
            double childFitness = calcFitness();
            System.out.println("Child fitness " + childFitness);

            // if childfitness > parentfitness copy oldagents to this.agents - max fitness is 0
            if (childFitness >= parentFitness) {

                updateAgents(oldAgents);
                updateColoniesFromPerant();
                System.out.println("Reverting to main.old population");
                for (int i = 0; i < agents.size(); i++) {
                    Agent x = agents.get(i);
                    x.printAgentString();

                }
                tempInterface.updateGrid(update());//rgen, maxDiffusionRate));
            } else {
                // updateColoniesFromPerant();
                System.out.println("Retaining child population");
                for (int i = 0; i < agents.size(); i++) {
                    Agent x = agents.get(i);
                    x.printAgentString();
                }
                parentFitness = childFitness;
                System.out.println("Retaining child agents");
                grid.printColourGrid();
               tempInterface.updateGrid(update());//rgen, maxDiffusionRate));

            }
            j++;
            //printTarget();
            // System.out.println("j is " + j + ", maxj is " + maxJ);
            // loop to mutate
            grid.resetMainGrid();
            resetAgentTag();
        }
        System.out.println(parentFitness);
        System.out.println("colour main.agent.Grid");
        grid.printColourGrid();
        System.out.println("numAgents");
        grid.printNumAgents();
        System.out.println("Final main.agent set");
        for (int i = 0; i < agents.size(); i++) {
            Agent x = agents.get(i);
            x.printAgentString();
        }
    }

    //constructor
    public Simulation() {

        commonConstructerElements();
    }

    // constructor
    public Simulation(Random rgen,
                      int numberOfSteps,
                      int numberOfTrials,
                      int maxJ,
                      double minFitness,
                      int gridSize,
                      int maxSignalPeptides,
                      int maxReporterPeptides,
                      int numberOfAgents,
                      int maxOutput,
                      int maxSensitivity,
                      int maxDiffusionRate,
                      int maxColours
                //      int maxColonyDepth
    ) {
        this.rgen = rgen;
        this.numberOfSteps = numberOfSteps;
        this.numberOfTrials = numberOfTrials;
        this.maxJ = maxJ;
        this.minFitness = minFitness;
        this.maxColonyDepth = maxColonyDepth;
        this.gridSize = gridSize;

        this.maxSignalPeptides = maxSignalPeptides;
        this.maxReporterPeptides = maxReporterPeptides;
        this.numberOfAgents = numberOfAgents;
        this.maxOutput = maxOutput;
        this.maxSensitivity = maxSensitivity;
        this.maxDiffusionRate = maxDiffusionRate;
        this.maxColours = maxColours;
        startingOutputPeptides = gridSize * gridSize;
        startingSignalPeptides = gridSize * gridSize;

        commonConstructerElements();
    }


    private void commonConstructerElements() {
        grid = new Grid(gridSize, this.maxColours);
        agents = new ArrayList<Agent>();
        for (int i = 0; i < numberOfAgents; i++) {
            Agent a = new Agent(rgen, gridSize, this.maxSensitivity, maxSignalPeptides, this.maxDiffusionRate, maxReporterPeptides,
                    this.maxColours, this.maxColonyDepth,2);
            agents.add(a);
            grid.updateGCAgentDensity(a.getXLoc(), a.getYLoc(), 1);
        }

        // initialise peptides at random or nothing will happen
        peptides = new ArrayList<Peptide>();
        for (int i = 0; i < startingSignalPeptides; i++) {
            Peptide p = new Peptide(rgen, this.maxDiffusionRate, "Signal", 0, gridSize);      // signal peptides don't have a colour
            peptides.add(p);
            grid.updateGCOSPDensity(p.getXloc(), p.getYloc(), 1);
        }
        for (int i = 0; i < startingOutputPeptides; i++) {
            int col = rgen.nextInt(this.maxColours);
            Peptide p = new Peptide(rgen, this.maxDiffusionRate, "Output", col, gridSize);
            peptides.add(p);
            grid.updateGCORPDensity(p.getXloc(), p.getYloc(), p.getColour(), 1);
        }

        // set up the target pattern - currently a French flag
        // TODO: Make this read in from a file
        target = new ArrayList<Integer>();
        int value = 0;
        for (int j = 0; j < gridSize; j++) {
            value = 0;
            for (int i = 0; i < gridSize / 3; i++) {
                target.add(value);
            }
            value++;
            for (int i = gridSize / 3; i < 2 * (gridSize / 3); i++) {
                target.add(value);
            }
            value++;
            for (int i = 2 * (gridSize / 3); i < gridSize; i++) {
                target.add(value);
            }
        }
    }

    // print target to stdout
    public void printTarget() {
        System.out.println("Target:");
        // System.out.print(target.get(0) + "\t");
        int pos = 0;
        for (int i = 0; i < gridSize * gridSize - 1; i++) {

            if (pos == gridSize) {
                System.out.println();
                pos = 1;
            } else {
                pos++;
            }
            System.out.print(target.get(i) + "\t");
        }
    }


    // print contents of EA to stdout
    public void print() {
        System.out.println("Agents: ");
        for (Agent nextAg : agents) {
            nextAg.printAgent();
            System.out.println();
        }
        System.out.println("Peptides: ");
        printPeptides();
        grid.printGrid();
    }

    // update all agents
    public int[][] update(){//Random rgen, int MAXDR) {

        for (int i = 0; i < agents.size(); i++) {
            Agent a = agents.get(i);

            // check if main.agent should die

            // produce peptides if neighbourhood signal peptide density is sufficient
            if (a.CheckToProducePeptides(grid)) { // peptides are produced in the cell when a.CheckToProducePeptides is called.
                int newOP = a.getNumOP();
                int ORPType = a.getORPType();           // ORPType is the same as colour; not sure I've used it consistently
                for (int j = 0; j < newOP; j++) {
                 //   Peptide np = new Peptide(rgen, MAXDR, "Output", ORPType, gridSize);//ORP type is in embeded in the Peptide.
                  //  np.setXLoc(a.getXLoc());
                  //  np.setYLoc(a.getYLoc());

                    // add the peptide to the master peptide list
                    //this.peptides.add(np);

                    // update the peptide concentrations on the grid
                    grid.updateGCOSPDensity(a.getXLoc(), a.getYLoc(), a.getNumSP());
                    grid.updateGCORPDensity(a.getXLoc(), a.getYLoc(), a.getORPType(), a.getNumOP());

                }
                // a.expirePeptide()
            }

            // move the main.agent in a random direction and update the grid
            // remove the main.agent from its current position
            //System.out.println("main.agent.Agent is at " + a.getYLoc() + " " + a.getYLoc());
            //System.out.println("main.agent.Agent density at this location is " + grid.getGCAgentDensity(a.getXLoc(), a.getYLoc()));
            // CW note to self change main.agent dencity to list of agents at a later date.
            this.grid.updateGCAgentDensity(a.getXLoc(), a.getYLoc(), -1);
            grid.updateGCORPDensity(a.getXLoc(), a.getYLoc(), a.getORPType(), a.getPeptideCount() * -1);

            a.move(rgen, gridSize  , grid, moveAttempts);
            //System.out.println("Moved, new main.agent density at this location is " + grid.getGCAgentDensity(a.getXLoc(), a.getYLoc()));
            // and add it to its new position
            this.grid.updateGCAgentDensity(a.getXLoc(), a.getYLoc(), 1);
            grid.updateGCORPDensity(a.getXLoc(), a.getYLoc(), a.getORPType(), a.getPeptideCount());
        }


        for (int i = 0; i < agents.size(); i++) {
            Agent a = agents.get(i);
            for (int t = 0; t < i; t++) {
                if (a.sharesGridWithAgent(agents.get(t))) {

                    a.move(rgen, gridSize , grid, moveAttempts);

                }
            }
            grid.updateGCAgentDensity(a.getXLoc(), a.getYLoc(), 1);
            grid.updateGCORPDensity(a.getXLoc(), a.getYLoc(), a.getORPType(), a.getPeptideCount());
        }
        // expire any peptides necessary
        for (int i = 0; i < peptides.size(); i++) {
            Peptide p = peptides.get(i);
            double decide = rgen.nextDouble();
            if (decide <= p.getExtinctionRate()) {
                // update grid
                int x = p.getXloc();
                int y = p.getYloc();
                String pType = p.getType();
                if (pType.equals("Signal")) {
                    grid.updateGCOSPDensity(x, y, -1);
                } else {
                    grid.updateGCORPDensity(x, y, p.getColour(), -1);
                }

                // remove from master peptide list
                peptides.remove(i);
            }
        }

        // diffuse peptides
        for (int i = 0; i < peptides.size(); i++) {
            Peptide p = peptides.get(i);
            // update grid to reflect new position - remove from main.old position
            grid.updateGCORPDensity(p.getXloc(), p.getYloc(), p.getColour(), -1);
            p.diffuse(grid.getGridSize(), rgen);
            // update grid to reflect new position - increment new position
            grid.updateGCORPDensity(p.getXloc(), p.getYloc(), p.getColour(), 1);
        }

        return grid.getColorArray(agents);
    }

    private void resetAgentTag() {
        for (int x = 0; x < gridSize; x++) {
            for (int y = 0; y < gridSize; y++) {
                grid.returnGridCell(x, y).setAgentInResidence(false);
            }

        }
        for (int i = 0; i < agents.size(); i++) {
            grid.returnGridCell(agents.get(i).getXLoc(), agents.get(i).getYLoc()).setAgentInResidence(true);
        }
    }

    // print a summary of the peptides in the system

    public void printPeptideSummary() {
        for (int i = 0; i < peptides.size(); i++) {
            Peptide p = peptides.get(i);
            int colr = p.getColour();
            System.out.println(colr);
        }
        System.out.println();
    }
/*
    public void printPeptideSummary(){
        int colour;
        int x;
        int y;
        int amount;
        for(int i = 0; i< agents.size();i++){
            x = agents.get(i).getXLoc();
            y = agents.get(i).getYLoc();
            colour = agents.get(i).getORPType();
            amount = agents.get(i).getPeptideCount();
            grid.updateGCORPDensity(x,y,colour,amount);
        }
    }*/

    // printPeptides
    public void printPeptides() {
        for (Peptide p : peptides) {
            p.printPeptide();
            System.out.println();
        }
    }

    // calculate the fitness of the grid relative to the target
    // fitness is root mean square of differences between the grid and the target
    public double calcFitness() {
        ArrayList<Integer> theColourMap = new ArrayList<Integer>();
        theColourMap = grid.getColourMap();
        double diff = 0.0;
        for (int i = 0; i < theColourMap.size(); i++) {
            diff += ((target.get(i) - theColourMap.get(i)) * (target.get(i) - theColourMap.get(i)));
        }
        diff /= target.size();
        return Math.sqrt(diff);
    }

    // copy existing agents to a new array
    public ArrayList<Agent> copyAgents() {
        ArrayList<Agent> toList = new ArrayList<Agent>();
        for (Agent a : agents) {
            Agent b = new Agent();
            a.copyAgent(b);
            toList.add(b);
        }
        return toList;
    }

    // mutate all of the agents
    public void mutate(Random rgen, int sensitivity, int motility, int numOSP, int numORP, int numColours) {
        ArrayList<Agent> oldAgents = new ArrayList<Agent>();
        for (Agent a : agents) {
            a.mutate(rgen, sensitivity, motility, numOSP, numORP, numColours);
        }
    }

    // print all agents to stdout
    public void printAgents() {
        for (int i = 0; i < agents.size(); i++) {
            Agent a = agents.get(i);
            a.printAgent();
            System.out.println();
        }
    }

    // print all agents to stdout in a tab-delimited string
    public void printAgentsString() {
        for (int i = 0; i < agents.size(); i++) {
            Agent a = agents.get(i);
            a.printAgentString();
        }
    }

    // update agents with values from an arrayList
    public void updateAgents(ArrayList<Agent> fromList) {
        // public void updateAgents(ArrayList<Agent> fromList) {
        for (int i = 0; i < this.agents.size(); i++) {
            Agent a = this.agents.get(i);
            Agent b = fromList.get(i);
            a.setSensitivity(b.getSensitivity());
            a.setMotility(b.getMotility());
            a.setXLoc(b.getXLoc());
            a.setYLoc(b.getYLoc());
            a.setNumOSP(b.getNumSP());
            a.setNumORP(b.getNumOP());
            a.setORPType(b.getORPType());
        }

    }

    // todo do a test for this methord
    public ArrayList<Agent> updateColoniesFromPerant() {
        ArrayList<ArrayList<Agent>> temp = returnLargeistColourColonies();//<------<< this calls a list of the largest colonies
        ArrayList<Agent> returnList = new ArrayList<Agent>();
        Random colourRandom = new Random(maxColours * 7);
        Random indexRandom = new Random(agents.size() * 5);
        Random posRandom = new Random((int) Math.pow(gridSize, 2));

        int tempIndex;
        int tempColour;
        int n = 0;
        for (int i = 0; i < maxColours; i++) {
            for (int j = 0; j < temp.get(i).size(); j++) {
                Agent newAgent = new Agent();
                temp.get(i).get(j).copyAgent(newAgent);
                returnList.add(newAgent);
                n++;
            }
        }
        for (int i = n; i < agents.size(); i++) {
            tempColour = colourRandom.nextInt(maxColours);
            tempIndex = indexRandom.nextInt(temp.get(tempColour).size());
            Agent newAgent = new Agent();
            temp.get(tempColour).get(tempIndex).copyAgent(newAgent);

            returnList.add(newAgent);
        }

        for (int i = 0; i < returnList.size(); i++) {

            returnList.get(i).setXLoc(posRandom.nextInt(gridSize));
            returnList.get(i).setYLoc(posRandom.nextInt(gridSize));
        }
        return returnList;
    }

    //todo make test for this methord
    public ArrayList<ArrayList<Agent>> returnLargeistColourColonies() {
        ArrayList<ArrayList<Agent>> temp = new ArrayList<ArrayList<Agent>>();
        Collections.sort(temp, new Comparator<ArrayList<Agent>>() {
            @Override
            public int compare(ArrayList<Agent> o1, ArrayList<Agent> o2) {
                return o1.size() - o2.size();
            }
        });
        ArrayList<ArrayList<Agent>> tempReturn = new ArrayList<ArrayList<Agent>>();
        Agent[][] tempAgentGrid = new Agent[gridSize][gridSize];
        for (int i = 0; i < agents.size(); i++) {
            tempAgentGrid[agents.get(i).getXLoc()][agents.get(i).getYLoc()] = new Agent();
            agents.get(i).copyAgent(tempAgentGrid[agents.get(i).getXLoc()][agents.get(i).getYLoc()]);
            //  System.out.println(tempAgentGrid[agents.get(i).getXLoc()][agents.get(i).getYLoc()].getORPType());
        }
        for (int i = 0; i < maxColours; i++) {
            for (int x = 0; x < gridSize; x++) {
                for (int y = 0; y < gridSize; y++) {
                    temp.add(clusterAgents(tempAgentGrid, x, y, i));

                }
            }

            int tempInt = 0;
            for (int j = 0; j < temp.size(); j++) {
                if (temp.get(tempInt).size() < temp.get(j).size()) {
                    tempInt = j;
                }
            }
            tempReturn.add(temp.get(tempInt));
            temp.clear();
        }
        return tempReturn;
    }


    public ArrayList<Agent> clusterAgents(Agent[][] agentList, int x, int y, int colour) {
        ArrayList<Agent> returningList = new ArrayList<Agent>();
        ArrayList<Agent> temp0 = new ArrayList<Agent>();
        ArrayList<Agent> temp1 = new ArrayList<Agent>();
        ArrayList<Agent> temp2 = new ArrayList<Agent>();
        ArrayList<Agent> temp3 = new ArrayList<Agent>();
        if (x >= gridSize || y >= gridSize || x < 0 || y < 0) {
            return returningList;
        }


        if (agentList[x][y] != null && agentList[x][y].getORPType() == colour) {
            Agent tempAgent = new Agent();
            agentList[x][y].copyAgent(tempAgent);

            returningList.add(tempAgent);
            agentList[x][y] = null;
            temp0 = clusterAgents(agentList, x + 1, y, colour);
            temp1 = clusterAgents(agentList, x, y + 1, colour);
            temp2 = clusterAgents(agentList, x - 1, y, colour);
            temp3 = clusterAgents(agentList, x, y - 1, colour);
            for (int i = 0; i < temp0.size(); i++) {
                returningList.add(temp0.get(i));
                //  temp0.remove(0);
            }
            for (int i = 0; i < temp1.size(); i++) {
                returningList.add(temp1.get(i));
                // temp1.remove(0);
            }
            for (int i = 0; i < temp2.size(); i++) {
                returningList.add(temp2.get(i));
                // temp2.remove(0);
            }
            for (int i = 0; i < temp3.size(); i++) {
                returningList.add(temp3.get(i));
                // temp2.remove(0);
            }
            // returninglist.add(agentList[x][y]);

        }

        return returningList;
    }

}


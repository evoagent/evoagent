package main.agent.movement;

import main.agent.Grid;
import main.agent.Peptide;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by chris on 7/09/2015.
 */
public class SearchPattern implements objectMovement {
    private int yLoc;
    private int xLoc;
    private int ORPType;
    private int direction = 0;
    public SearchPattern(int x, int y, int ORPin) {

        ORPType = ORPin;
    }

    @Override
    public int callMove(Random rgen, int gridSize, Grid gridIn, int attempts, int motility, int xIn, int yIn, int Direction) {
        xLoc = xIn;
        yLoc = yIn;
        int tempChampion = 0;
        int champX = 0;
        int champY = 0;
        int tempPlace = 0;
        int directionalRotator;
/*
        for (int y = this.yLoc - 1; y <= this.yLoc + 1; y++) {
            for (int x = this.xLoc - 1; x <= this.yLoc + 1; x++) {
                if (x >= 0 && y >= 0 && x < gridSize && y < gridSize && (x != this.xLoc && y != this.yLoc)) {
                    if (gridIn.returnGridCell(x, y).getStrongestRepoterPeptide() == this.ORPType) {
                        tempPlace = gridIn.returnGridCell(x, y).getConc("", this.ORPType);
                        if (tempChampion < tempPlace) {
                            tempChampion = tempPlace;
                            champX = x;
                            champY = y;
                        }
                    }
                }
            }
        }
        if (tempChampion > 0) {
            xLoc = champX;
            yLoc = champY;
            for (int y = this.yLoc - 1; y <= this.yLoc + 1; y++) {
                for (int x = this.xLoc - 1; x <= this.yLoc + 1; x++) {
                    if (x >= 0 && y >= 0 && x < gridSize && y < gridSize && (x != this.xLoc && y != this.yLoc)) {
                        gridIn.returnGridCell(x, y).incReporterPeptideConc(ORPType, rgen.nextInt(motility * 2 + 1));
                    }
                }
            }

        }*/
        directionalRotator = rgen.nextInt(3) - 1;
        if (rgen.nextInt(12) == 0) {
            return (Direction + 2) %4 ;
        } else {
            return (direction += directionalRotator) % 4;
        }
    }

    @Override
    public Pos2D getPosition() {
        return null;
    }



    @Override
    public ArrayList<Peptide> producePeptides() {
        return null;
    }

    @Override
    public void mutate() {

    }
}

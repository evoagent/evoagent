package main.agent.movement;

import main.agent.Grid;
import main.agent.Peptide;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by chris on 7/09/2015.
 */
public class Foo implements objectMovement {
    private Pos2D position;
    private ArrayList<Peptide> productionPeptides = new ArrayList<Peptide>();

    @Override
    public int callMove(Random radomIn, int gridSize, Grid gridIN, int attempts, int Motility, int x, int y, int Direction) {
        return 0; // return 0 to 3 for directional control or 4 for exact x y coordinates;
    }

    @Override
    public Pos2D getPosition() {
        return null; //return a custom coordinate to implement the position we want on the grid of the agent.
    }



    @Override
    public ArrayList<Peptide> producePeptides() {
        return null;
    }

    @Override
    public void mutate() {

    }
}

package main.agent.movement;

import main.agent.Grid;
import main.agent.Peptide;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by chris on 7/09/2015.
 */
public interface objectMovement {


    public int callMove(Random radomIn, int gridSize, Grid gridIN, int attempts, int Motility, int x, int y, int Direction);

    public Pos2D getPosition();

    public ArrayList<Peptide> producePeptides();

    public void mutate();
}


package main.agent.movement;

import main.agent.Grid;
import main.agent.Peptide;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by chris on 7/09/2015.
 */
public class slowPassBy implements objectMovement {
    private ArrayList<Peptide> productionPeptides = new ArrayList<Peptide>();
    private int Direction;
    private int ORPType;
    private int yLoc;
    private int xLoc;
    private int colonyDepthSearch;
    private int colony;
    private int gridSize;

    public slowPassBy(int orpType, int colonyDepthIn, int colonyIn, int gridSizeIn) {
        ORPType = orpType;
        colonyDepthSearch = colonyDepthIn;
        colony = colonyIn;
        gridSize = gridSizeIn;
    }

    @Override
    public int callMove(Random radomIn, int gridSize, Grid gridIN, int attempts, int Motility, int x, int y, int Direction) {
        Direction += radomIn.nextInt() % 4;
        for (int SX = x - 1; SX < x + 1; SX++) {
            for (int SY = y - 1; SY < y + 1; SY++) {
                if (SX >= 0 && SY >= 0 && SX < gridSize && SY < gridSize && (SX != x && SY != y)) {
                    if (gridIN.returnGridCell(SX, SY).getColouroOfAgent() == this.ORPType) {
                        movement(Direction, gridIN, gridSize);
                        return 4;
                    }


                }

            }

        }
        return Direction;
    }

    @Override
    public Pos2D getPosition() {
        Pos2D temp = new Pos2D();
        temp.setCords(this.xLoc, this.yLoc);
        return temp;
    }


    @Override
    public ArrayList<Peptide> producePeptides() {
        return null;
    }

    @Override
    public void mutate() {

    }

    private void movement(int Direction, Grid gridIn, int gridSize) {
        int newX = 0;
        int newY = 0;
        switch (Direction) {

            case 0: // up
                newY = this.yLoc - 1; //+ attempts;
                if (newY < 0) {
                    break;
                    //newY = 0;
                    //   } else if (gridIn.returnGridCell(this.xLoc,newY).returnAgentInResidance()) {
                    //       attempts++;
                    //       this.move(rgen, gridSize, gridIn, attempts);
                    //      break;
                }
                if (!checkForColony(gridIn)) {
                    this.yLoc = newY;
                }
                break;
            case 1:// left
                newX = this.xLoc - 1; //+ attempts;
                if (newX < 0) {
                    break;
                    //newX = 0;
                    //    } else if (gridIn.returnGridCell(newX,this.yLoc).returnAgentInResidance()) {
                    //       attempts++;
                    //       this.move(rgen, gridSize, gridIn, attempts);
                    //     break;
                }
                if (!checkForColony(gridIn)) {
                    this.xLoc = newX;
                }
                break;

            case 2:// down
                newY = this.yLoc + 1;// - attempts;
                if (newY >= gridSize) {
                    break;
                    // newY = gridSize;
                    // } else if  (gridIn.returnGridCell(this.xLoc,newY).returnAgentInResidance()) {
                    //    attempts++;
                    //    this.move(rgen, gridSize, gridIn, attempts);
                    //   break;
                }
                if (!checkForColony(gridIn)) {
                    this.yLoc = newY;
                }
                break;

            case 3:// right
                newX = this.xLoc + 1;
                if (newX >= gridSize) {
                    break;
                    //  newX = gridSize;
                    //   } else if (gridIn.returnGridCell(newX,this.yLoc).returnAgentInResidance()) {
                    //      attempts++;
                    //      this.move(rgen, gridSize, gridIn, attempts);
                    //    break;
                }
                if (!checkForColony(gridIn)) {
                    this.xLoc = newX;
                }
        }

    }

    public boolean checkForColony(Grid gridIn) {
        int temp = colonySearch(gridIn, xLoc, yLoc, colonyDepthSearch);
        // int temp = gridIn.theGrid[this.xLoc][this.yLoc].
        if (temp >= colony) {
            return true;
        }
        return false;
    }

    private int colonySearch(Grid gridIn, int x, int y, int colonyDepthDecrement) {
        int temp = 0;
        if (x >= gridSize || y >= gridSize || x < 0 || y < 0 || colonyDepthDecrement <= 0) {
            return temp;
        }
        if (gridIn.getSignalPeptideConc(x, y) != ORPType) {
            return temp;
        }

        temp++;

        //    temp += colonySearch(gridIn, x - 1, y + 1);
        temp += colonySearch(gridIn, x, y + 1, colonyDepthDecrement - 1);
        //   temp += colonySearch(gridIn, x + 1, y - 1);
        temp += colonySearch(gridIn, x - 1, y, colonyDepthDecrement - 1);
        temp += colonySearch(gridIn, x + 1, y, colonyDepthDecrement - 1);
        //    temp += colonySearch(gridIn, x - 1, y + 1);
        temp += colonySearch(gridIn, x, y + 1, colonyDepthDecrement - 1);
        //    temp += colonySearch(gridIn, x + 1, y + 1);

        return temp;
    }
}

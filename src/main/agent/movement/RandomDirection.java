package main.agent.movement;

import main.agent.Grid;
import main.agent.Peptide;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by chris on 7/09/2015.
 */
public class RandomDirection implements objectMovement {
    @Override
    public int callMove(Random radomIn, int gridSize, Grid gridIN, int attempts, int Motility, int x, int y, int Direction) {
        return radomIn.nextInt() % 4;
    }

    @Override
    public Pos2D getPosition() {
        return null;
    }



    @Override
    public ArrayList<Peptide> producePeptides() {
        return null;
    }

    @Override
    public void mutate() {

    }
}

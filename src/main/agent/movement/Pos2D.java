package main.agent.movement;

/**
 * Created by chris on 7/09/2015.
 */

public class Pos2D {
    private int xOrd;
    private int yOrd;

    public Pos2D() {
    }

    public void setCords(int x, int y) {
        xOrd = x;
        yOrd = y;
    }

    public void setXOrd(int x) {
        xOrd = x;
    }

    public void setYOrd(int y) {
        yOrd = y;
    }

    public int getXord() {
        return xOrd;
    }

    public int getYOrd() {
        return yOrd;
    }

}

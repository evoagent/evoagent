package main.agent;
/*-------------------------------------------------------------------------------------------------------------------*\
|                                                       Class Grid                                                    |
|                                       The landscape upon which the agents live                                      |
|                                          Square, currently no wraparound                                            |
|                                             Author: Jennifer Hallinan                                               |
|                                               Commenced: 19/10/2014                                                 |
|                                              Last edited: 28/04/2014                                                |
\*-------------------------------------------------------------------------------------------------------------------*/

import java.io.*;
import java.util.ArrayList;

public class Grid {
    // unit testing
    public static void main(String[] args) {
        Grid g = new Grid(10, 3);
        g.printGrid();
    }

    // constructor
    public Grid(int gridSize, int numColours) {
        this.theGrid = new gridCell[gridSize][gridSize];
        this.gridSize = gridSize;
        // not really necessary to initialize, but I'm a c programmer
        for (int x = 0; x < gridSize; x++) {
            for (int y = 0; y < gridSize; y++) {
                this.theGrid[x][y] = new gridCell(numColours);
            }
        }
    }

    // print grid to stdout
    public void printGrid() {
        printAgentGrid();
        printSignalGrid();
        printReporterGrid();
        printColourGrid();
    }

    /**
     * return number of agents, broken down by colour
     */
    public void printNumAgents() {
        int numBlue = 0;
        int numWhite = 0;
        int numRed = 0;
        for (int x = 0; x < this.gridSize; x++) {
            // System.out.println("Row: " + i);
            for (int y = 0; y < this.gridSize; y++) {
                // System.out.println("Column: " + j);
                gridCell gc = this.theGrid[x][y];
                // System.out.println("Blue: " + gc.getConc("Reporter", 0));
                numBlue += gc.getConc("Reporter", 0);
                // System.out.println("White: " + gc.getConc("Reporter", 1));
                numWhite += gc.getConc("Reporter", 1);
                // System.out.println("Red: " + gc.getConc("Reporter", 2));
                numRed += gc.getConc("Reporter", 2);
            }
        }
        System.out.println("Total number of agents; blue: " + numBlue + ", white: " + numWhite + ", red: " + numRed);
        System.out.println("Total: " + (numBlue + numWhite + numRed));
    }

    /**
     * print agent density in a tab-delimited grid
     */

    public void printAgentGrid() {
        int ad;
        System.out.println("Agents");
        for (int i = 0; i < this.gridSize; i++) {
            for (int j = 0; j < this.gridSize; j++) {
                gridCell gc = this.theGrid[i][j];
                ad = gc.getAgentDensity();// this declares a new "ad" verible for each time in the loop
                System.out.print(ad + "\t");
            }
            System.out.println();
        }
    }

    /**
     * writes the list of agents to a specfic file destionation
     *
     * @param filename the name of the file to be written to.
     */
    public void printAgentGrid(String filename) {
        int ad;
        File outfile = new File(filename);
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outfile)));
            out.println("Agents");
            for (int i = 0; i < this.gridSize; i++) {
                for (int j = 0; j < this.gridSize; j++) {
                    gridCell gc = this.theGrid[i][j];
                    ad = gc.getAgentDensity();
                    out.print(ad + "\t");
                }
                out.println();
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * print signal peptide density grid
     */

    public void printSignalGrid() {
        System.out.println();
        System.out.println("Signal peptides");
        for (int x = 0; x < this.gridSize; x++) {
            for (int y = 0; y < this.gridSize; y++) {
                gridCell gc = this.theGrid[x][y];
                int sp = gc.getSignalPeptideConc();
                System.out.print(sp + "\t");
            }
            System.out.println();
        }
    }

    /**
     * prints the Signal grid to a file
     *
     * @param filename file to be written to
     */
    public void printSignalGrid(String filename) {
        int sp;
        File outfile = new File(filename);
        try {
            PrintWriter sout = new PrintWriter(new BufferedWriter(new FileWriter(outfile)));
            sout.println("Signal peptides");
            for (int x= 0; x < this.gridSize; x++) {
                for (int y = 0; y < this.gridSize; y++) {
                    gridCell gc = this.theGrid[x][y];
                    sp = gc.getSignalPeptideConc();
                    sout.print(sp + "\t");
                }
                sout.println();
            }
            sout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * print reporter grid
     */

    public void printReporterGrid() {
        int rp;
        System.out.println();
        System.out.println("Reporter peptides");
        for (int i = 0; i < this.gridSize; i++) {
            for (int j = 0; j < this.gridSize; j++) {
                gridCell gc = this.theGrid[i][j];
                for (int k = 0; k < gc.getNumColours(); k++) {
                    rp = gc.getConc("Reporter", k);
                    System.out.print(rp + ";");
                }
                System.out.print("\t");
            }
            System.out.println();
        }
    }

    public void printReporterGrid(String filename) {
        int rp;
        File outfile = new File(filename);
        try {
            PrintWriter rout = new PrintWriter(new BufferedWriter(new FileWriter(outfile)));
            for (int i = 0; i < this.gridSize; i++) {
                for (int j = 0; j < this.gridSize; j++) {
                    gridCell gc = this.theGrid[i][j];
                    for (int k = 0; k < gc.getNumColours(); k++) {
                        rp = gc.getConc("Reporter", k);
                        rout.print(rp + ";");
                    }
                    rout.print("\t");
                }
                rout.println();
            }
            rout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * print colour map
     */

    public void printColourGrid() {
        int maxval;
        System.out.println();
        System.out.println("Maximum colour");
        for (int i = 0; i < this.gridSize; i++) {
            for (int j = 0; j < this.gridSize; j++) {
                gridCell gc = this.theGrid[i][j];
                maxval = -1;
                for (int k = 0; k < gc.getNumColours(); k++) {
                    int num0 = gc.getConc("Reporter", 0);
                    int num1 = gc.getConc("Reporter", 1);
                    int num2 = gc.getConc("Reporter", 2);
                    if (num0 > num1 && num0 > num2) {
                        maxval = 0;
                    }
                    if (num1 > num0 && num1 > num2) {
                        maxval = 1;
                    }
                    if (num2 > num0 && num2 > num1) {
                        maxval = 2;
                    }
                }
                System.out.print(maxval + "\t");
            }
            System.out.println();
        }
    }

    /**
     * return colourmap as an ArrayList so it can be used in calcFitness
     */

    public ArrayList<Integer> getColourMap()

    // got to check that is a correct use of the operators in the if statement
    // Also what happens if there is an equal amount of peptides of two colours and if they are both higher then the other type giving no result for this type.
    // This will also this will break if there is more then three types of peptides.

    {
        int num0;
        int num1;
        int num2;
        ArrayList colourMap = new ArrayList<Integer>();
        int maxval = -1;//  this will only return -1 if there are values of X = Y > Z or any combination of X Y Z where two of these vlaues are the highest compaired to each other.
        for (int i = 0; i < this.gridSize; i++) {
            for (int j = 0; j < this.gridSize; j++) {
                gridCell gc = this.theGrid[i][j];
                maxval = getMaxval(maxval, gc);
                colourMap.add(maxval);
            }
        }
        return (colourMap);
    }

    /**
     * gets the array of the colour
     *
     * @return
     */

    public int[][] getColorArray(ArrayList<Agent> temp) {
        int[][] tempArray = new int[gridSize][gridSize];
        for (int x = 0; x < gridSize; x++) {
            for (int y = 0; y < gridSize; y++) {
                tempArray[x][y] = -1;
            }
        }

        for (int t = 0; t < temp.size(); t++)

        {
            if (tempArray[temp.get(t).getXLoc()][temp.get(t).getYLoc()] == -1) {
                tempArray[temp.get(t).getXLoc()][temp.get(t).getYLoc()] = temp.get(t).getORPType();
            }
            /*else
            {
                for(int u = t ;u > 0 ;u --){
                    if ((temp.get(t).getXLoc()== temp.get(u).getXLoc())&&( temp.get(t).getYLoc() ==temp.get(t).getYLoc())){
                        tempArray[temp.get(t).getXLoc()][temp.get(t).getYLoc()] = Math.max(temp.get(t).)
                    }
                }
            }*/
        }
        //  for (int x = 0; x < gridSize; x++) {
        //     for (int y = 0; y < gridSize; y++) {
        //          tempArray[x][y] = this.theGrid[x][y].getStrongestRepoterPeptide();
        //      }

        return tempArray;
    }

    /**
     * this returns the max value in the
     *
     * @param maxval
     * @param gc
     * @return
     */
    private int getMaxval(int maxval, gridCell gc) {
        int num0;
        int num1;
        int num2;
        for (int k = 0; k < gc.getNumColours(); k++) {
            num0 = gc.getConc("Reporter", 0);
            num1 = gc.getConc("Reporter", 1);
            num2 = gc.getConc("Reporter", 2);
            if (num0 > num1 && num0 > num2) {
                maxval = 0;
            }
            if (num1 > num0 && num1 > num2) {
                maxval = 1;
            }
            if (num2 > num0 && num2 > num1) {
                maxval = 2;
            }
        }
        return maxval;
    }


    public void printColourGrid(String filename) {
        int rp;
        File outfile = new File(filename);
        try {
            PrintWriter cout = new PrintWriter(new BufferedWriter(new FileWriter(outfile)));
            for (int i = 0; i < this.gridSize; i++) {
                for (int j = 0; j < this.gridSize; j++) {
                    gridCell gc = this.theGrid[i][j];
                    int max = -1;
                    for (int k = 0; k < gc.getNumColours(); k++) {
                        rp = gc.getConc("Reporter", k);
                        if (rp > max && rp > 0) {
                            max = k;
                        }
                    }
                    cout.print(max + "\t");
                }
                cout.println();
            }
            cout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * return grid size
     */

    public int getGridSize() {
        return (this.gridSize);
    }

    /**
     * return the concentration of a specific peptide at a specific grid location
     */

    public int getSignalPeptideConc(int row, int col) {
        gridCell gc = this.theGrid[row][col];
        return (gc.getSignalPeptideConc());
    }

    /**
     * return agent density at a specific grid location
     */
    public int getGCAgentDensity(int row, int col) {
        gridCell gc = this.theGrid[row][col];
        return gc.getAgentDensity();
    }

    /**
     * update values for a grid cell
     */

    public void updateGCAgentDensity(int row, int col, int howmany) {
        this.theGrid[row][col].incAgentDensity(howmany);
    }

    public void updateGCOSPDensity(int row, int col, int howmany) {
        this.theGrid[row][col].incSignalPeptideConc(howmany);
    }

    public void updateGCORPDensity(int row, int col, int which, int howmany) {
        this.theGrid[row][col].incReporterPeptideConc(which, howmany);
    }

    public boolean isGridSpaceFull(int x, int y) {
    return theGrid[x][y].returnAgentInResidance();
    }

    public gridCell returnGridCell(int x, int y) {
        return this.theGrid[x][y];
    }

    public void resetMainGrid() {
        for (int x = 0; x < this.gridSize; x++) {
            for (int y = 0; y < this.gridSize; y++) {
                updateGCAgentDensity(x, y, -this.theGrid[x][y].getAgentDensity());
                for (int i = 0; i < this.theGrid[x][y].getNumColours(); i++) {
                    updateGCORPDensity(x, y, i, -this.theGrid[x][y].getConc("Reporter", i));

                }
            }
        }
    }

    // private variables
    public gridCell[][] theGrid;
    private int gridSize;
}


package main.agent;

import java.util.ArrayList;
/*-------------------------------------------------------------------------------------------------------------------*\
|                                              Class main.agent.gridCell                                                         |
|                           An element of a grid; keeps track of its contents                                         |
|                                      Author: Jennifer Hallinan                                                      |
|                                       Commenced: 19/10/2014                                                         |
|                                      Last edited: 23/03/2014                                                        |
\*-------------------------------------------------------------------------------------------------------------------*/

public class gridCell {

    // unit test
    public static void main(String[] args) {
        gridCell gc = new gridCell(3);
        gc.printCell();
        gc.incAgentDensity(3);
        gc.incReporterPeptideConc(0, 2);
        gc.incSignalPeptideConc(4);
        gc.printCell();
    }

    // constructor
    public gridCell(int numColours) {
        this.agentDensity = 0;
        this.signalPeptideConc = 0;
        this.reporterPeptideConc = new int[numColours];
    }

    public void addPeptides(Peptide peptide) {
        this.peptideList.add(peptide);
    }


    // gets
    public int getAgentDensity() {
        return (this.agentDensity);
    }

    public int getSignalPeptideConc() {
        return (this.signalPeptideConc);
    }

    //public int[] getReporterPeptideConc(){return (this.reporterPeptideConc);}
    public int getNumColours() {
        return (this.reporterPeptideConc.length);
    }

    // increments
    public void incAgentDensity(int howmany) {
        this.agentDensity += howmany;
    }

    public void incSignalPeptideConc(int howmany) {
        this.signalPeptideConc += howmany;
    }

    public void incReporterPeptideConc(int which, int howmany) {
        this.reporterPeptideConc[which] += howmany;
    }

    // print main.agent.gridCell to stdout
    public void printCell() {
        System.out.println("main.agent.Agent density: " + this.agentDensity);
        System.out.println("Signal peptide concentration:" + this.signalPeptideConc);
        for (int i = 0; i < this.reporterPeptideConc.length; i++) {
            System.out.println("Reporter peptide concentration: " + i + " " + this.reporterPeptideConc[i]);
        }
    }

    // return concentration of a specific species
    public int getConc(String type, int which) {
        int val = 0;
        if (type.equals("Signal")) {
            val = this.signalPeptideConc;
        } else {
            val = this.reporterPeptideConc[which];
        }
        return (val);
    }

    public void setAgentInResidence(boolean boolSet) {
        agentInResidence = boolSet;
    }

    public boolean returnAgentInResidance() {
        return agentInResidence;
    }

    public void setColouroOfAgent(int ORPType) {
        colouroOfAgent = ORPType;
    }

    public int getColouroOfAgent() {
        return colouroOfAgent;
    }

    public int getStrongestRepoterPeptide() {

        int champion = 0;
        int temp = 0;
        for (int i = 0; i < reporterPeptideConc.length; i++) {
            if (reporterPeptideConc[champion] <= reporterPeptideConc[i]) {
                champion = i;
                temp = reporterPeptideConc[i];
            }
        }
        if (temp == 0) {
            return -1;
        }
        return champion;
    }

    // public lists to be used in the future.

    // public ArrayList<Peptide> PeptideList = new ArrayList<Peptide>();
    // public ArrayList<Agent> AgentList = new ArrayList<Agent>();
    private boolean agentInResidence = false;
    private int colouroOfAgent = -1;
    // private variables
    private int agentDensity;                   // number of agents on the cell
    private int signalPeptideConc;              // number of molecules of each type of signal peptide
    private int reporterPeptideConc[];            // number of molecules of each type of reporter peptide
    private ArrayList<Peptide> peptideList = new ArrayList<Peptide>();

}

package main.agent;//import com.sun.org.apache.xpath.internal.operations.Bool;

import java.text.DecimalFormat;
import java.util.Random;

/*-------------------------------------------------------------------------------------------------------------------*\
|                                               Class main.agent.Peptide                                                      |
|                 Defines the characteristics of a signal peptide for communicating between cells                     |
|                                         Author: Jennifer Hallinan                                                   |
|                                          Commenced: 19/10/2014                                                      |
|                                         Last edited: 05/04/2015                                                     |
\*-------------------------------------------------------------------------------------------------------------------*/

public class Peptide
{
    // unit testing
    public static void main(String[] args)
    {
        Random rgen = new Random(666);
        for (int i = 0; i < 5; i++)
        {
            Peptide p = new Peptide(rgen, 1, "Signal", 1, 5);
            p.printPeptide();
        }
    }

    // constructor
    public Peptide(Random rgen, int DR, String type, int colour, int gridSize)
    {
        this.type = type;
        this.diffusionRate = DR;
        this.extinctionRate = rgen.nextDouble();
        this.xLoc = rgen.nextInt(gridSize-1);
        this.yLoc = rgen.nextInt(gridSize-1);
        this.colour = colour;
    }
    public Peptide(Random rgen, int DR, String type, int colour, int posX ,int posY)
    {
        this.type = type;
        this.diffusionRate = DR;
        this.extinctionRate = rgen.nextDouble();
        this.xLoc = posX ;
        this.yLoc = posY ;
        this.colour = colour;
    }
    // gets and sets
    public int getDiffusionRate()
    {
        return (this.diffusionRate);
    }
    public double getExtinctionRate()
    {
        return (this.extinctionRate);
    }

    // print peptide to stdout
    public void printPeptide()
    {
        System.out.println("main.agent.Peptide type: " + this.type);
        System.out.println("Colour: " + this.colour);
        System.out.print(" X location: " + this.xLoc);
        System.out.print(" Y Location: " + this.yLoc);
        System.out.print(" main.agent.Peptide diffusion rate: " + this.diffusionRate);
        DecimalFormat df = new DecimalFormat("#.##");
        System.out.println(" main.agent.Peptide extinction rate: " + df.format(this.extinctionRate));
    }

    // move a peptide - note there is no stochasticity in this bit atm
    public void diffuse(int gridSize, Random rgen)
    {
        // decide direction - only four direct neighbours

        int direction = rgen.nextInt(2342039);
        // System.out.println("New direction is " + direction);
        int newX = 0;
        int newY = 0;
        // System.out.println("Current X is " + this.xLoc + " current Y is " + this.yLoc);
        switch(direction%4)
        {
            case 0:                     // left
                newY = this.yLoc - this.diffusionRate;
                if (newY < 0)
                {
                    newY = 0;
                }
                // System.out.println("Moving left, newY is " + newY);
                this.yLoc = newY;
                break;
            case 1:                     // right
                newY = this.yLoc + this.diffusionRate;
                if (newY >= gridSize)
                {
                    newY = gridSize-1;
                }
                // System.out.println("Moving right, newY is " + newY);
                this.yLoc = newY;
                break;
            case 2:                     // up
                newX = this.xLoc - this.diffusionRate;
                if (newX < 0)
                {
                    newX = 0;
                }
                // System.out.println("Moving ip, newX is " + newX);
                this.xLoc = newX;
                break;
            case 3:                     // down
                newX = this.xLoc + this.diffusionRate;
                if (newX >= gridSize)
                {
                    newX = gridSize-1;
                }
                // System.out.println("Moving down, newX is " + newX);
                this.xLoc = newX;
        }
        // System.out.println();
    }

    // set row location
    public void setXLoc(int where)
    {
        this.xLoc = where;
    }

    // set column location
    public void setYLoc(int where)
    {
        this.yLoc = where;
    }

    // return peptide type
    public String getType()
    {
        return this.type;
    }

    // return row
    public int getXloc()
    {
        return this.xLoc;
    }

    // return column
    public int getYloc()
    {
        return this.yLoc;
    }

    // return colour
    public int getColour()
    {
        return this.colour;
    }

    // private variables
    private String type;                // signal peptide or output peptide
    private int diffusionRate;          // number of cells diffused per unit time
    private double extinctionRate;      // probability of disappearing per time step (0.0 - 1.0)
    private int xLoc;                   // what row the peptide is in
    private int yLoc;                   // what column the peptide is in
    private int colour;                 // what colour, if it is an output peptide. Null is 99
}


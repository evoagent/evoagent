package main.agent.PeptideProduction;

import main.agent.Peptide;

import java.util.ArrayList;

/**
 * Created by chris on 9/09/2015.
 */
public interface returnPeptide {

  public ArrayList<Peptide> producePeptide(Peptide peptideIn , int concentration, ArrayList<Peptide> peptideListIn);

  public void mutateValues();

}

package main.agent;/*-------------------------------------------------------------------------------------------------------------------*\
|                                                    Class main.agent.Agent                                                      |
|                       Individual agents which interact via defined input/output channels                            |
|                                           Author: Jennifer Hallinan                                                 |
|                                             Commenced: 19/10/2014                                                   |
|                                            Last edited: 10/07/2015                                                  |
|                                           edited by Chris Walker 10/07/2015                                         |
\*-------------------------------------------------------------------------------------------------------------------*/

import main.agent.movement.*;

import java.util.ArrayList;
import java.util.Random;

public class Agent {
    // TODO: Generalise peptides to have a variable number of input species

    // unit testing


    public static void main(String[] args) {
        Random rgen = new Random(666);
        for (int i = 0; i < 1; i++) {
            Agent a = new Agent(rgen, 51, 10, 5, 25, 5, 3, 4, 3);
            System.out.println();
            System.out.println("main.agent.Agent a");
            a.printAgent();
            System.out.println();
            Agent b = new Agent();
            System.out.println("main.agent.Agent b");
            b.printAgent();
            a.copyAgent(b);
            System.out.println("After copy");
            b.printAgent();
            System.out.println();

			/*
             * a.mutate(rgen, 10, 10, 5, 5, 3); System.out.println("Mutated:");
			 * a.printAgent();
			 */
        }
    }

    /**
     * Constuctor :
     * this constucts the agent and generates a random value for each Agent
     *
     * @param rgen           random seed
     * @param gridSize       the size of the grid being used
     * @param maxSensitivity how sensitive the Agent is to each signal peptide
     * @param maxNumOSP
     * @param maxDR          maximum motility for the Agent.
     * @param maxNumORP
     * @param numColours     the number of colours that can exist in the simulation.
     */
    public Agent(Random rgen, int gridSize, int maxSensitivity, int maxNumOSP,
                 int maxDR, int maxNumORP, int numColours, int maxColonyDepth, int moveType) {
        this.xLoc = (rgen.nextInt(gridSize) + rgen.nextInt(gridSize)) % gridSize;
        this.yLoc = (rgen.nextInt(gridSize) + rgen.nextInt(gridSize)) % gridSize;
        this.sensitivity = rgen.nextInt(maxSensitivity); // to make sure actual
        // sensitivity is <
        // max
        this.motility = rgen.nextInt(maxDR); // agents have motility and
        // peptides do too
        // at the moment they are the
        // same
        this.numOSP = rgen.nextInt(maxNumOSP);
        this.numORP = rgen.nextInt(maxNumORP);
        this.ORPType = rgen.nextInt(numColours); // which output reporter
        // peptide is produced?
        this.colonyDepthSearch = rgen.nextInt(maxColonyDepth);
        this.deathProb = rgen.nextDouble() / 2.0; // probability of death per
        // timestep
        this.RandomGen = rgen;
        this.sizeOfGrid = gridSize;
        switch (moveType) {
            case 0:
                movement = new SearchPattern(this.xLoc, this.yLoc, this.ORPType);
                break;
            case 1:
                movement = new RandomDirection();
                break;
            case 2:
                movement = new slowPassBy(this.ORPType, colonyDepthSearch, colony, gridSize);
                break;
            case 3:
                movement = new compleateRandom();
                break;
        }
    }

    // constructor
    public Agent() {
        this.xLoc = 0;
        this.yLoc = 0;
        this.sensitivity = 0;
        this.motility = 0;
        this.numOSP = 0;
        this.numORP = 0;
        this.ORPType = 0;
        this.deathProb = 0.0;
    }

    // print main.agent details to stdout

    /**
     * prints agent internal values
     */
    public void printAgent() {
        System.out.println("x location:" + this.xLoc);
        System.out.println("y location:" + this.yLoc);
        System.out.println("Sensitivity: " + this.sensitivity);
        System.out.println("Motility: " + this.motility);
        System.out
                .println("Number of signal peptides produced: " + this.numOSP);
        System.out.println("Number of reporter peptides produced: "
                + this.numORP);
        System.out.println("Reporter peptide produced: " + this.ORPType);
        System.out.println("Probability of death: " + this.deathProb);
    }

    /**
     * print main.agent to stdout as a string
     */

    public void printAgentString() {
        System.out.print(this.xLoc + "\t" + this.yLoc + "\t" + this.sensitivity
                + "\t" + this.motility + "\t");
        System.out.println(this.numOSP + "\t" + this.numORP + "\t"
                + this.ORPType);
    }


    /**
     * update the main.agent - location and peptide production
     *
     * @param rgen     random value neede for cacluating movement direction
     * @param gridSize sets grid boundries
     * @param gridIn   map of grid spaces for the comparison movement and colonisation algorithm
     * @param attempts decremental countdown for recursion
     */
    public void move(Random rgen, int gridSize, Grid gridIn, int attempts) {
        gridIn.returnGridCell(xLoc, yLoc).setAgentInResidence(false);
        gridIn.returnGridCell(xLoc, yLoc).setColouroOfAgent(-1);
        int newX = 0;
        int newY = 0;
        int temp;
        if (this.motility == 0) {
            temp = 0;
        } else {
            temp = rgen.nextInt(this.motility) + 1;
        }
        switch (movement.callMove(rgen, gridSize, gridIn, attempts, motility, xLoc, yLoc, direction)) {

            case 0:// left
                newX = this.xLoc - temp; //+ attempts;
                if (newX < 0) {
                    break;
                    //newX = 0;
                    //    } else if (gridIn.returnGridCell(newX,this.yLoc).returnAgentInResidance()) {
                    //       attempts++;
                    //       this.move(rgen, gridSize, gridIn, attempts);
                    //     break;
                }
                if (!checkForColony(gridIn)) {
                    this.xLoc = newX;
                }
                break;

            case 1:// down
                newY = this.yLoc + temp;// - attempts;
                if (newY >= gridSize) {
                    break;
                    // newY = gridSize;
                    // } else if  (gridIn.returnGridCell(this.xLoc,newY).returnAgentInResidance()) {
                    //    attempts++;
                    //    this.move(rgen, gridSize, gridIn, attempts);
                    //   break;
                }
                if (!checkForColony(gridIn)) {
                    this.yLoc = newY;
                }
                break;

            case 2:// right
                newX = this.xLoc + temp;//- attempts;
                if (newX >= gridSize) {
                    break;
                    //  newX = gridSize;
                    //   } else if (gridIn.returnGridCell(newX,this.yLoc).returnAgentInResidance()) {
                    //      attempts++;
                    //      this.move(rgen, gridSize, gridIn, attempts);
                    //    break;
                }
                if (!checkForColony(gridIn)) {
                    this.xLoc = newX;
                }
                break;
            case 3: // up
                newY = this.yLoc - temp; //+ attempts;
                if (newY < 0) {
                    break;
                    //newY = 0;
                    //   } else if (gridIn.returnGridCell(this.xLoc,newY).returnAgentInResidance()) {
                    //       attempts++;
                    //       this.move(rgen, gridSize, gridIn, attempts);
                    //      break;
                }
                if (!checkForColony(gridIn)) {
                    this.yLoc = newY;
                }
                break;
            case 4:
                xLoc = movement.getPosition().getXord();
                yLoc = movement.getPosition().getYOrd();
                break;

        }

        for (int t = 0; t < this.peptideAcumiation.size(); t++) {

            this.peptideAcumiation.get(t).setXLoc(this.getXLoc());
            this.peptideAcumiation.get(t).setYLoc(this.getYLoc());

        }
        gridIn.returnGridCell(xLoc, yLoc).setAgentInResidence(true);
        gridIn.returnGridCell(xLoc, yLoc).setColouroOfAgent(this.ORPType);
        return;
    }


    private boolean detectInTheBoundry(int temp) {
        if ((temp >= 0) || (temp <= this.sizeOfGrid)) {
            return true;
        }

        return false;
    }

    /**
     * checks if there is a colony in the ajacent squares and returns a boolean value
     *
     * @param gridIn the grid to check
     * @return true if there is a colony or false if there is not a colony
     */
    public boolean checkForColony(Grid gridIn) {
        int temp = colonySearch(gridIn, xLoc, yLoc, colonyDepthSearch);
        // int temp = gridIn.theGrid[this.xLoc][this.yLoc].
        if (temp >= colony) {
            return true;
        }
        return false;
    }

    private int colonySearch(Grid gridIn, int x, int y, int colonyDepthDecrement) {
        int temp = 0;
        temp++;
        if (x >= sizeOfGrid || y >= sizeOfGrid || x < 0 || y < 0 || colonyDepthDecrement <= 0) {
            return temp;
        }
        if (gridIn.getSignalPeptideConc(x, y) != ORPType) {
            return temp;
        }


        //    temp += colonySearch(gridIn, x - 1, y + 1);
        temp += colonySearch(gridIn, x, y + 1, colonyDepthDecrement - 1);
        //   temp += colonySearch(gridIn, x + 1, y - 1);
        temp += colonySearch(gridIn, x - 1, y, colonyDepthDecrement - 1);
        temp += colonySearch(gridIn, x + 1, y, colonyDepthDecrement - 1);
        //    temp += colonySearch(gridIn, x - 1, y + 1);
        temp += colonySearch(gridIn, x, y - 1, colonyDepthDecrement - 1);
        //    temp += colonySearch(gridIn, x + 1, y + 1);

        return temp;
    }

    /**
     * this dictates the death of a peptide with in the cell.
     *
     * @param peptideDeathProbablitlty
     * @return
     */
    public int expirePeptide(double peptideDeathProbablitlty) {
        int temp = 0;
        for (int t = 0; t < peptideAcumiation.size(); t++) {
            if (peptideAcumiation.get(t).getExtinctionRate() >= peptideDeathProbablitlty) {
                peptideAcumiation.remove(t);
                peptideCount--;
                temp++;

            }
        }
        return temp;
    }

    /**
     * do I produce peptides - four-neighbourhood
     * This class produces a peptide within the agent if it needs to and returns a boolean if it needs to or not.
     *
     * @param g grid in to check if there is an peptide within the vicinity
     * @return true if it produces a peptide or false if it does not.
     */

    public boolean CheckToProducePeptides(Grid g) {
        boolean createNewPeptide;
        int numSP = 0;
        // System.out.println("main.agent.Agent location X: " + this.getXLoc() + " Y: " +
        // this.getYLoc());

        // left
        int nextY = this.getYLoc() - 1;
        // System.out.println("X: " + this.getXLoc() + " Y: " + nextY);
        if (nextY >= 0 && nextY < g.getGridSize()) {
            // System.out.println("Getting signal peptide concentration");
            numSP += g.getSignalPeptideConc(this.getXLoc(), nextY);
        }

        // right
        nextY = this.getYLoc() + 1;
        // System.out.println("X: " + this.getXLoc() + " Y: " + nextY);
        if (nextY >= 0 && nextY < g.getGridSize()) {
            // System.out.println("Getting signal peptide concentration");
            numSP += g.getSignalPeptideConc(this.getXLoc(), nextY);
        }

        // up
        int nextX = this.getXLoc() - 1;
        // System.out.println("X: " + nextX + " Y: " + this.getYLoc());
        if (nextX >= 0 && nextX < g.getGridSize()) {
            // System.out.println("Getting signal peptide concentration");
            numSP += g.getSignalPeptideConc(nextX, this.getYLoc());
        }

        // down
        nextX = this.getXLoc() + 1;
        // System.out.println("X: " + nextX + " Y: " + this.getYLoc());
        if (nextX >= 0 && nextX < g.getGridSize()) {
            // System.out.println("Getting signal peptide concentration");
            numSP += g.getSignalPeptideConc(nextX, this.getYLoc());
        }

        createNewPeptide = numSP >= this.sensitivity;

        if (createNewPeptide)// this only returns a boolean not peptides. Where do new Peptides come from? CW
        {

            peptideCount++;
            //  Peptide newPeptide = new Peptide(this.RandomGen, this.getMotility(), "Reporter", this.getORPType(), this.xLoc, this.yLoc);
            // peptideAcumiation.add(newPeptide);
        }

        return createNewPeptide;
    }

    public ArrayList<Peptide> movemnentPeptideProduction() {
        return movement.producePeptides();
    }

    /**
     * return number of signal peptides to produce
     */

    public int getNumSP() {
        return this.numOSP;
    }

    /**
     * return number of output reporter peptides to produce
     */

    public int getNumOP() {
        return this.numORP;
    }

    /**
     * return row location
     */

    public int getXLoc() {
        return this.xLoc;
    }

    /**
     * return column location
     */

    public int getYLoc() {
        return this.yLoc;
    }

    /**
     * return type of output reporter peptide
     */

    public int getORPType() {
        return this.ORPType;
    }

    /**
     * returns the minimum sensitivity value to the signaling peptide of the agent
     *
     * @return returns the integer value of the ORP type
     */
    public int getSensitivity() {
        return this.sensitivity;
    }

    /**
     * returns the motility value of the agent
     *
     * @return Returns the integer value of the motility in squares
     */
    public int getMotility() {
        return this.motility;
    }

    public double getDeathProb() {
        return this.deathProb;
    }

    public int getPeptideCount() {//        return this.peptideAcumiation.size();
        return this.peptideCount;
    }

    public ArrayList<Peptide> getPeptideAcumiation() {
        return this.peptideAcumiation;
    }

    // mutate one of the parameters
    public void mutate(Random rgen, int sensitivity, int motility, int numOSP,
                       int numORP, int numColours) {
        int which = rgen.nextInt(7); // five parameters which can be mutated
        switch (which) {
            case 0:
                int delta = rgen.nextInt(sensitivity * 2) - sensitivity; // want
                // negative
                // and
                // positive
                // values
                if (delta > 0) {
                    this.sensitivity += delta;
                }
                break;
            case 1:
                delta = rgen.nextInt(motility * 2) - motility;
                if (delta > 0) {
                    this.motility += delta;
                }
                break;
            case 2:
                this.numOSP = rgen.nextInt(numOSP);
                break;
            case 3:
                this.numORP = rgen.nextInt(numORP);
                break;
            case 4:
                this.ORPType = rgen.nextInt(numColours);
                break;
            case 6:
                movement.mutate();
                break;
            case 7:

        }
    }

    public boolean sharesGridWithAgent(Agent compareAgent) {
        if ((this.xLoc == compareAgent.xLoc) && (this.yLoc == compareAgent.yLoc)) {
            return true;
        }
        return false;
    }

    // sets
    public void setXLoc(int inX) {
        this.xLoc = inX;
    }

    public void setYLoc(int inY) {
        this.yLoc = inY;
    }

    public void setSensitivity(int inSens) {
        this.sensitivity = inSens;
    }

    public void setMotility(int inMot) {
        this.motility = inMot;
    }

    public void setNumOSP(int inNum) {
        this.numOSP = inNum;
    }

    public void setNumORP(int inNum) {
        this.numORP = inNum;
    }

    public void setORPType(int inType) {
        this.ORPType = inType;
    }

    public void setDeathProb(double inProb) {
        this.deathProb = inProb;
    }

    // copy my values to a new main.agent
    public void copyAgent(Agent a) {
        a.setXLoc(this.xLoc);
        a.setYLoc(this.yLoc);
        a.setSensitivity(this.sensitivity);
        a.setMotility(this.motility);
        a.setNumOSP(this.numOSP);
        a.setNumORP(this.numORP);
        a.setORPType(this.ORPType);
        a.setDeathProb(this.deathProb);
    }


    public Boolean testAgentCopy(Agent AgentToCompare) {
        if (AgentToCompare.xLoc != this.xLoc) {
            return false;
        }
        if (AgentToCompare.yLoc != this.yLoc) {
            return false;
        }
        if (AgentToCompare.sensitivity != this.sensitivity) {
            return false;
        }
        if (AgentToCompare.motility != this.motility) {
            return false;
        }
        if (AgentToCompare.numOSP != this.numOSP) {
            return false;
        }
        if (AgentToCompare.numORP != this.numORP) {
            return false;
        }
        if (AgentToCompare.ORPType != this.ORPType) {
            return false;
        }
        if (AgentToCompare.deathProb != this.deathProb) {
            return false;
        }

        return true;
    }

    public boolean isHere(int x, int y) {
        if ((this.xLoc == x) && (this.yLoc == y)) {
            return true;
        }
        return false;
    }

    // private variables
    private int colony = 3;
    private int colonyDepthSearch = 3;
    private int peptideCount = 0;
    private Random RandomGen;
    private int xLoc; // row of grid
    private int yLoc; // column of grid
    private int sensitivity; // min sensitivity of promoter to input molecule
    // number
    private int direction;
    private ArrayList<Peptide> peptideAcumiation = new ArrayList<Peptide>();
    private ArrayList<Peptide> externalTempList = new ArrayList<Peptide>();
    private int sizeOfGrid;
    private int motility; // how many cells does it move in a timestep?
    private int numOSP; // number of signal peptides to produce
    private int numORP; // number of output peptides to produce
    private int ORPType; // type of output reporter peptide produced
    private double deathProb; // probability of dying at every timestep
    private objectMovement movement;


}


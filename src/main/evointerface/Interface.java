package main.evointerface;

import main.evointerface.panels.AnimationGrid;
import main.evointerface.panels.InfoPanel;
import main.evointerface.panels.ParameterPanel;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.HashMap;

/**
 * Created by daniels on 12/07/2015.
 */
public class Interface {

    // mainFrame is the main window (JFrame), and it contains
    // mainPanel (the top level JPanel), which in turns contains
    // the three smaller panels (parameter, animation, and info panel)
    private JFrame mainFrame;
    private JPanel mainPanel;

    private ParameterPanel parameterPanel;
    private InfoPanel infoPanel;
    private AnimationGrid animationGrid;

    // the start and reset buttons on the mainPanel
    public JButton startButton;
    public JButton resetButton;

    private int gridSize;

    private int DEFAULT_GRID_SIZE = 100;

    public Interface()
    {

        // Create the top level panel
        MigLayout parentLayout = new MigLayout("fillx,filly,insets 20, gap 5");
        mainPanel = new JPanel(parentLayout);

        // Create the sub panels
        animationGrid = new AnimationGrid(DEFAULT_GRID_SIZE,1);
        parameterPanel = new ParameterPanel(DEFAULT_GRID_SIZE);
        this.gridSize = DEFAULT_GRID_SIZE;
        infoPanel = new InfoPanel();
        JPanel buttonPanel = createButtonPanel();

        mainPanel.add(parameterPanel, "growy, span 1 1");
        JTabbedPane animationPanel = new JTabbedPane();
        animationPanel.add("Animation",animationGrid);
        mainPanel.add(animationPanel, "grow, push, span 1 2");
        mainPanel.add(infoPanel, "growy, wrap, span 1 2");
        mainPanel.add(buttonPanel, "growx");


        parameterPanel.gridSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                JSlider slider = (JSlider) evt.getSource();
                if (!slider.getValueIsAdjusting()) {
                    changeGridSize(slider.getValue());
                    gridSize = slider.getValue();
                }
            }
        });

        // Place all the components in to the JFrame and display it
        mainFrame = new JFrame();
        mainFrame.pack();
        mainFrame.getContentPane().add(mainPanel);
        mainFrame.setSize(1425,860);
        mainFrame.setVisible(true);
    }


    public HashMap<String,String> getInputValues(){
        HashMap<String,String> values = parameterPanel.getParameterValues();
        values.put("gridsize", Integer.toString(gridSize));

        return values;
    }

    public void changeGridSize(int newGridSize)
    {
        if (newGridSize == 0)
            newGridSize = 1;

        gridSize = newGridSize;
        animationGrid.setGridSize(gridSize);
        animationGrid.repaint();
    }

    public void updateGrid(int[][] cellInfo)
    {
        Color[][] cellColors = new Color[gridSize][gridSize];
        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                switch (cellInfo[i][j]) {
                    case -1:
                        cellColors[i][j] = Color.black;
                        break;
                    case 0:
                        cellColors[i][j] = Color.red;
                        break;
                    case 1:
                        cellColors[i][j] = Color.white;
                        break;
                    case 2:
                        cellColors[i][j] = Color.blue;
                }
            }
        }
        animationGrid.updateCellInfo(cellColors);
        System.out.println("updateGrid in EDT: " + SwingUtilities.isEventDispatchThread());
        try {
            Thread.sleep(30);
        }
        catch (Exception e){
        }
        animationGrid.repaint();
    }

    private JPanel createButtonPanel()
    {
        MigLayout buttonLayout = new MigLayout("fill, wrap 2, w 200, h 120");
        JPanel panel = new JPanel(buttonLayout);

        startButton = new JButton("Start");
        startButton.setMnemonic('s');

        panel.add(startButton, "span, split 2, growx, h 100");
        JButton stopButton = new JButton("Reset");
        panel.add(stopButton, "growx, h 100");

        return panel;
    }

    public HashMap<String,String> getParameterValues()
    {
        return parameterPanel.getParameterValues();
    }




}



package main.evointerface.panels;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;

/**
 * Created by daniels on 9/08/2015.
 */
public class InfoPanel extends JTabbedPane {

    public InfoPanel()
    {
        this.add("Info",createInfoPanel());
    }

    private JPanel createInfoPanel(){
        JPanel panel = new JPanel(new MigLayout("fillx, wrap 1, w 240"));

        return panel;
    }
}

package main.evointerface.panels;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.HashMap;

/**
 * Created by daniels on 9/08/2015.
 */
public class ParameterPanel extends JTabbedPane{


    public JSlider gridSlider;


    public HashMap<String,String> getParameterValues()
    {
        HashMap<String,String> values = new HashMap<String,String>();

        values.put("signal_peptides",tf_NumberOfSignalPeptides.getText());
        values.put("output_peptides",tf_NumberOfOutputPeptides.getText());
        values.put("no_of_agents",tf_NumberOfAgents.getText());
        values.put("max_output",tf_MaxOutput.getText());
        values.put("max_sensitivity",tf_MaxSensitivity.getText());
        values.put("max_diffusion",tf_MaxDiffusion.getText());
        values.put("max_colours",tf_MaxColours.getText());

        values.put("random_seed",tf_RandomSeed.getText());
        values.put("steps",tf_NumberOfSteps.getText());
        values.put("trials",tf_NumberOfTrials.getText());
        values.put("maxJ",tf_MaxJ.getText());
        values.put("min_fitness",tf_MinFitness.getText());
        values.put("grid_size",Integer.toString(gridSlider.getValue()));

        return values;
    }

    public ParameterPanel(int startingGridSize) {
        this.startingGridSize = startingGridSize;
        this.add("General", createGeneralParameterPanel());
        this.add("Agent", createAgentParameterPanel());
    }


    // Private Variables
    // General parameters -- note: the variables are prefixed with 'tf_' to denote that they are JTextField
    private JTextField tf_RandomSeed,
            tf_NumberOfSteps,
            tf_NumberOfTrials,
            tf_MaxJ,
            tf_MinFitness;


    // Agent Parameters
    private JTextField tf_NumberOfSignalPeptides,
            tf_NumberOfOutputPeptides,
            tf_NumberOfAgents,
            tf_MaxOutput,
            tf_MaxSensitivity,
            tf_MaxDiffusion,
            tf_MaxColours;

    private int startingGridSize;

    // Private Methods
    private JPanel createGeneralParameterPanel() {
        JPanel panel = new JPanel(new MigLayout("fillx, wrap 2, w 240"));

        panel.add(new JLabel("Random seed:"));
        tf_RandomSeed = new JTextField("9999");
        panel.add(tf_RandomSeed, "w 80, align right");

        panel.add(new JLabel("Number of steps:"));
        tf_NumberOfSteps = new JTextField("500");
        panel.add(tf_NumberOfSteps, "span 2, w 80, align right");

        panel.add(new JLabel("Number of trials:"));
        tf_NumberOfTrials = new JTextField("1000");
        panel.add(tf_NumberOfTrials, "span 2, w 80, align right");

        panel.add(new JLabel("Max J:"));
        tf_MaxJ = new JTextField("10");
        panel.add(tf_MaxJ, "span 2, w 80, align right");

        panel.add(new JLabel("Min Fitness:"));
        tf_MinFitness = new JTextField("0.5");
        panel.add(tf_MinFitness, "span 2, w 80, align right");

        gridSlider = new JSlider(JSlider.HORIZONTAL, 0, 200, startingGridSize);
        gridSlider.setMinorTickSpacing(10);
        gridSlider.setMajorTickSpacing(50);
        gridSlider.setPaintTicks(true);
        gridSlider.setPaintLabels(true);
        gridSlider.setSnapToTicks(true);
        panel.add(gridSlider, "span 2, center, w 240");



        return panel;
    }

    private JPanel createAgentParameterPanel()
    {
        JPanel panel = new JPanel(new MigLayout("fillx, wrap 2, w 240"));

        panel.add(new JLabel("Number of Signal Peptides:"),"wrap");
        tf_NumberOfSignalPeptides = new JTextField("100");
        panel.add(tf_NumberOfSignalPeptides, "span, grow, w 80, align left");

        panel.add(new JLabel("Number of Output Peptides:"),"wrap");
        tf_NumberOfOutputPeptides = new JTextField("100");
        panel.add(tf_NumberOfOutputPeptides, "span, grow, w 80, align left");

        panel.add(new JLabel("Number of Agents:"),"wrap");
        tf_NumberOfAgents = new JTextField("20");
        panel.add(tf_NumberOfAgents, "span, grow, w 80, align left");

        panel.add(new JLabel("Maximum output:"),"wrap");
        tf_MaxOutput = new JTextField("1000");
        panel.add(tf_MaxOutput, "span, grow, w 80, align left");

        panel.add(new JLabel("Maximum sensitivity:"),"wrap");
        tf_MaxSensitivity = new JTextField("3");
        panel.add(tf_MaxSensitivity, "span, grow, w 80, align left");

        panel.add(new JLabel("Maximum diffusion rate:"),"wrap");
        tf_MaxDiffusion = new JTextField("5");
        panel.add(tf_MaxDiffusion, "span, grow, w 80, align left");

        panel.add(new JLabel("Maximum number of colours:"),"wrap");
        tf_MaxColours = new JTextField("3");
        panel.add(tf_MaxColours, "span, grow, w 80, align left");

        return panel;
    }

}

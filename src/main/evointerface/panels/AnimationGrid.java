package main.evointerface.panels;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by daniels on 9/08/2015.
 */

/* AnimationPanel is the JFrame which contains the animation of the agents
 *
 */
public class AnimationGrid extends JPanel {

    private static int gridSize;
    private static int gridGap;

    private static Color[][] cellInfo;

    public AnimationGrid(int gridSize, int gridGap){
        setGridSize(gridSize);
        this.gridGap = gridGap;

        JPanel panel = new JPanel(new MigLayout("align center, fillx, wrap 1"));


        cellInfo = new Color[gridSize][gridSize];
        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
                cellInfo[i][j] = new Color(128,128,128);
    }

    private Graphics2D canvas;


    @Override
    protected void paintComponent(Graphics g) {

        int panelWidth = getWidth();
        int panelHeight = getHeight();

        int cellWidth = panelWidth/getGridSize() - gridGap;
        int cellHeight = panelHeight/getGridSize() - gridGap;
        int xOffset = cellWidth + gridGap;
        int yOffset = cellHeight + gridGap;

        super.paintComponent(g);

        canvas = (Graphics2D)g;
        canvas.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                canvas.setColor(getCellColor(i,j));
                //canvas.setColor(Color.LIGHT_GRAY);
                canvas.fillRect(i * xOffset, j * yOffset, cellWidth, cellHeight);
            }
        }
        canvas.fillRect(0, 0, cellWidth, cellHeight);
    }

    public void setGridSize(int gridSize) {
        this.gridSize = gridSize;
        cellInfo = new Color[gridSize][gridSize];
        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
                cellInfo[i][j] = new Color(128,128,128);
    }

    public int getGridSize(){
        return gridSize;
    }

    public void updateCellInfo(Color[][] newCellInfo){
        cellInfo = new Color[getGridSize()][getGridSize()];
        for(int i = 0; i < getGridSize(); i++)
            for(int j = 0; j < getGridSize(); j++) {
                cellInfo[i][j] = newCellInfo[i][j];
            }
        repaint();
    }

    public Color getCellColor(int i, int j)
    {
        return cellInfo[i][j];
    }



}

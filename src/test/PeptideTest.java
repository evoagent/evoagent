package test;

import main.agent.Grid;
import main.agent.Peptide;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by chris on 22/06/2015.
 */
public class PeptideTest {
    private int randomNumberSeed = 147;
    private Random tempLong = new Random(randomNumberSeed);
    private int diffutionRate = 1;
    private int gridSize = 5;
    private int testInt = 2;
    private int maxSensitivity = 5;
    private int maxNumOSP = 5;
    private int maxNumORP = 5;
    private int numColours = 3;
    private Grid TempGrid = new Grid(gridSize, numColours);
    private int colour = 2;
    private String peptideType = new String("Signal");


    Peptide newPeptide = new Peptide(tempLong, diffutionRate, peptideType,
            colour, gridSize);


    // testing to see if the peptide stays with in the boundaries of


    // testing the setting of the position of the peptide


    // testing diffuse algorithm staying within bounds of the grid

    @org.junit.Test
    public void testMain() throws Exception {

    }

    @Test
    public void testGetDiffusionRate() throws Exception {
        assertEquals(diffutionRate, newPeptide.getDiffusionRate());
    }

    @Test
    public void testGetExtinctionRate() throws Exception {
        if ((newPeptide.getExtinctionRate() <= randomNumberSeed)
                || (newPeptide.getExtinctionRate() >= 0)) {
            assertEquals(1, 1);
        } else {
            assertEquals(1, 2);
        }
    }

    @Test
    public void testPrintPeptide() throws Exception {
// this is not neccerry to test for.
    }

    @Test
    public void testDiffuse() throws Exception {

        newPeptide.diffuse(gridSize, tempLong);
        if ((newPeptide.getXloc() <= gridSize) || (newPeptide.getXloc() >= 0)) {
            assertEquals(1, 1);
        } else {
            assertEquals(1, 2);
        }
        if ((newPeptide.getYloc() <= gridSize) || (newPeptide.getYloc() >= 0)) {
            assertEquals(1, 1);
        } else {
            assertEquals(1, 2);
        }

    }

    @Test
    public void testSetXLoc() throws Exception {
        newPeptide.setXLoc(testInt);
        assertEquals("this tests the setting of X location in the peptide", newPeptide.getXloc(), testInt);

    }

    @Test
    public void testSetYLoc() throws Exception {

        newPeptide.setYLoc(testInt);

        assertEquals("this Tests the position of the peptide in the Y plane", newPeptide.getYloc(), testInt);

    }

    @Test
    public void testGetType() throws Exception {
        assertEquals("This Tests that the main.agent.Peptide is the right type ", newPeptide.getType(), "Signal");
    }

    @Test
    public void testGetXloc() throws Exception {
        newPeptide.setXLoc(testInt);
        assertEquals("this tests that the Peptides X location is in the Correct position", newPeptide.getXloc(), testInt);
    }

    @Test
    public void testGetYloc() throws Exception {
        newPeptide.setYLoc(testInt);
        assertEquals("this tests that the Peptides Y location is in the Correct position", newPeptide.getYloc(), testInt);
    }

    @Test
    public void testGetColour() throws Exception {

        assertEquals("this tests that the system has the correct colour", newPeptide.getColour(), colour);
    }

    @Test
    public void testBoudries() {
        // this tests the boundries of the peptide;
        if ((newPeptide.getXloc() <= gridSize) || (newPeptide.getXloc() >= 0)) {
            assertEquals(1, 1);
        } else {
            assertEquals(1, 2);
        }
        if ((newPeptide.getYloc() <= gridSize) || (newPeptide.getYloc() >= 0)) {
            assertEquals(1, 1);
        } else {
            assertEquals(1, 2);
        }
    }
}
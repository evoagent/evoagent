package test;

import main.agent.Agent;
import main.agent.Grid;
import main.agent.Peptide;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by chris on 22/06/2015.
 */
public class AgentTest {
    int RandomSeed = 666;
    Random testRandom = new Random(RandomSeed);
    int gridSizeX = 10;
    int gridSizeY = 10;
    int gridSizeSquare = 10;
    int maxSensitivity = 5;
    int maxNumOSP = 5;
    int maxNumORP = 5;
    int numColours = 3;
    int colonyDepth = 3;
    int runBehaviourType = 1;
    Grid testGrid = new Grid(gridSizeSquare, numColours);

    Agent TestAgent = new Agent(testRandom, gridSizeX, gridSizeY, maxSensitivity, maxNumOSP, maxNumORP, numColours, colonyDepth,runBehaviourType );

    @org.junit.Test
    public void testMain() throws Exception {

    }

    @Test
    public void testPrintAgent() throws Exception {

    }

    @Test
    public void testPrintAgentString() throws Exception {

    }

    @Test
    public void testMove() throws Exception {
        // this tests if the main.agent has moved


        Random randomMove = new Random(RandomSeed);
        int[] countArray = {0, 0};
        int[] startingPointArray = {TestAgent.getXLoc(), TestAgent.getYLoc()};
        int attemps = 0;
        for (int x = 0; x < 20000; x++) {
            TestAgent.move(randomMove, gridSizeX, testGrid, attemps);
            if (startingPointArray[0] != TestAgent.getXLoc()) {//this tests the X direction Motility
                countArray[0] += 1;
            }

            if (startingPointArray[1] != TestAgent.getYLoc()) {//this tests the Y direction Motility
                countArray[1] += 1;
            }
        }
        //   assertNotEquals("this tests if the main.agent.Agent has moved in the X direction", 0, countArray[0]);
        //   assertNotEquals("this tests if the main.agent.Agent has moved in the Y direction", 0, countArray[1]);

        // testGrid.printAgentGrid();

    }

    @Test
    public void testProducePeptides() throws Exception {
        int attemps = 0;
        ArrayList testBool = new ArrayList<Boolean>();
        int testPos = 0;
        int testNeg = 0;
        ArrayList peptideArray = new ArrayList<Peptide>();
        int initalPeptideDensity = 0;
        int subtiquentPeptideDencity = 0;
        int initalPeptidePopulation = 200;
        int testItterationAmount = 2000;
        for (int x = 0; x < initalPeptidePopulation; x++) {
            Peptide newPeptide = new Peptide(testRandom, 4, "Signal", 0, gridSizeSquare);
            peptideArray.add(newPeptide);
            testGrid.updateGCOSPDensity(newPeptide.getXloc(), newPeptide.getYloc(), 1);
        }
        for (int x = 0; x < testGrid.getColourMap().size(); x++) {
            initalPeptideDensity += testGrid.getColourMap().indexOf(x);
        }


        for (int x = 0; x < testItterationAmount; x++) {
            TestAgent.move(testRandom, 3, testGrid, attemps);
            TestAgent.CheckToProducePeptides(testGrid);
           /* if (TestAgent.peptideCount() > testPos) {
                testPos++;
            } else {
                testNeg++;
            }
  */

        }

/*
for (int x = 0 ;x < testGrid.getColourMap().size();x++) {
    subtiquentPeptideDencity += testGrid.getColourMap().indexOf(x);
}


        System.out.println(initalPeptideDensity);
        System.out.println(subtiquentPeptideDencity);
        assertNotEquals("checks if peptides have been produced",initalPeptideDensity,subtiquentPeptideDencity);
*/
        // System.out.println("this is the amount of Positve tests " + testPos);
        // System.out.println("this is the amount of Negitive tests "+ testNeg);

        //  assertNotEquals("This checks if the amount of peptides are not being produced every time", testItterationAmount, TestAgent.getPeptideCount());
        // assertNotEquals("this checks the amount of peptides produced are not 0", TestAgent.getPeptideCount(), 0);
        //  assertEquals("This tests that the amount of positive and negative results are equal to the total number of the testing" +
        //          "itterations ", testItterationAmount, testPos + testNeg);
    }

    @Test
    public void testGetNumSP() throws Exception {
        boolean tempBool = false;

        int temp = TestAgent.getPeptideCount();
        for (int x = 0; x < testGrid.getGridSize(); x++) {
            for (int y = 0; y < testGrid.getGridSize(); y++) {
                testGrid.updateGCOSPDensity(x, y, 15);
            }
        }
        for (int x = 0; x < 3000; x++) {
            tempBool = TestAgent.CheckToProducePeptides(testGrid);
        }
        //   assertNotEquals("checks if the main.agent has produced peptides to its list", temp, TestAgent.getPeptideCount());
        assertTrue("testes if CheckToProducePeptides returns true", tempBool);

    }

    @Test
    public void testGetNumOP() throws Exception {

    }

    @Test
    public void testGetXLoc() throws Exception {

    }

    @Test
    public void testGetYLoc() throws Exception {

    }

    @Test
    public void testGetORPType() throws Exception {

    }

    @Test
    public void testGetSensitivity() throws Exception {

    }

    @Test
    public void testGetMotility() throws Exception {

    }

    @Test
    public void testGetDeathProb() throws Exception {

    }

    @Test
    public void testMutate() throws Exception {
        // int tempSencitivity = TestAgent.getSensitivity();
        // int tempMotility = TestAgent.getMotility();
        Agent test = new Agent();

        TestAgent.copyAgent(test);
        assertTrue("tests if each main.agent is equal at the begining of the test process", TestAgent.testAgentCopy(test));
        test.mutate(testRandom, maxSensitivity, maxSensitivity, maxNumOSP, maxNumORP, numColours);
        assertFalse("tests of there has been a mutation", TestAgent.testAgentCopy(test));

    }

    @Test
    public void testSetXLoc() throws Exception {

    }

    @Test
    public void testSetYLoc() throws Exception {

    }

    @Test
    public void testSetSensitivity() throws Exception {

    }

    @Test
    public void testSetMotility() throws Exception {

    }

    @Test
    public void testSetNumOSP() throws Exception {

    }

    @Test
    public void testSetNumORP() throws Exception {

    }

    @Test
    public void testSetORPType() throws Exception {

    }

    @Test
    public void testSetDeathProb() throws Exception {

    }

    @Test
    public void testCopyAgent() throws Exception {
        Agent test = new Agent();
        Agent FalseAgent = new Agent();
        TestAgent.copyAgent(test);
        assertTrue("Testing the test", test.testAgentCopy(test));//test the Test true
        assertFalse("testing the Test", TestAgent.testAgentCopy(FalseAgent));// tests the test False
        assertTrue("Test CopyAgent", TestAgent.testAgentCopy(test));

    }
    @Test
    public void testAgentColonyDepth(){

    }
}
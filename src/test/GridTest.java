package test;

import main.agent.Agent;
import main.agent.Grid;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by chris on 22/06/2015.
 */
public class GridTest {


    int randomSeed = 600;
    Random testRandom = new Random(randomSeed);
    int maxSencitvity = 15;
    int GridSize = 10;
    int maxNumOSP = 5;
    int maxDR = 5;
    int maxNumORP = 5;
    int colours = 3;
    int listLength = 20;
    int colonyDepth = 3;
    int runBehaviourType = 1;
    public Grid testGrid = new Grid(GridSize, colours);

    private static ArrayList<Agent> Testlist = new ArrayList<Agent>();

    @org.junit.Test
    public void testMain() throws Exception {
        for (int x = 0; x < listLength; x++) {
            Agent TestAgent = new Agent(testRandom, maxSencitvity, GridSize, maxNumOSP, maxDR, maxNumORP, colours,colonyDepth,runBehaviourType);
            Testlist.add(TestAgent);
        }
    }

    @Test
    public void testPrintGrid() throws Exception {

    }

    @Test
    public void testPrintNumAgents() throws Exception {

    }

    @Test
    public void testPrintAgentGrid() throws Exception {

    }

    @Test
    public void testPrintAgentGrid1() throws Exception {

    }

    @Test
    public void testPrintSignalGrid() throws Exception {

    }

    @Test
    public void testPrintSignalGrid1() throws Exception {

    }

    @Test
    public void testPrintReporterGrid() throws Exception {

    }

    @Test
    public void testPrintReporterGrid1() throws Exception {

    }

    @Test
    public void testPrintColourGrid() throws Exception {

    }

    @Test
    public void testGetColourMap() throws Exception {
    //error

    }

    @Test
    public void testPrintColourGrid1() throws Exception {

    }

    @Test
    public void testGetGridSize() throws Exception {

    }

    @Test
    public void testGetSignalPeptideConc() throws Exception {

    }

    @Test
    public void testGetGCAgentDensity() throws Exception {
        int testSpotX = 5;
        int testSpotY = 5;
        int testAmount = 7;
        assertEquals(testGrid.getGCAgentDensity(testSpotX, testSpotY), 0);
        testGrid.updateGCAgentDensity(testSpotX, testSpotY, testAmount);
        assertEquals(testGrid.getGCAgentDensity(testSpotX, testSpotY), testAmount);

    }

    @Test
    public void testUpdateGCAgentDensity() throws Exception {
        int testSpotX = 5;
        int testSpotY = 5;
        int testAmount = 7;
        assertEquals(testGrid.getGCAgentDensity(testSpotX, testSpotY), 0);
        testGrid.updateGCAgentDensity(testSpotX, testSpotY, testAmount);
        assertEquals(testGrid.getGCAgentDensity(testSpotX, testSpotY), testAmount);

    }

    @Test
    public void testUpdateGCOSPDensity() throws Exception {


    }

    @Test
    public void testUpdateGCORPDensity() throws Exception {

    }
}